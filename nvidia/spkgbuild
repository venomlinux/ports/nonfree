# description	: NVIDIA Linux Display Driver (Latest Production Branch)
# maintainer	: Venom Linux Team, venomlinux at disroot dot org
# homepage	: https://www.nvidia.com/ 
# depends	: dkms gtk3 nv-codec-headers egl-wayland

name=nvidia
version=570.86.16
release=1
source="$name-$version.run::http://us.download.nvidia.com/XFree86/Linux-x86_64/$version/NVIDIA-Linux-x86_64-$version.run
	buildmodules-nvidia.sh
	prime-run
	10-nvidia-drm-outputclass.conf"

build() {
	install_lib() {	install -Dm0755 $1 $PKG/usr/lib/$2/${1##*/}; }
	install_bin() {	install -Dm0755 $1 $PKG/usr/bin/$2/${1##*/}; }
	install_file() { install -Dm${3:-0644} $1 $PKG/$2/${1##*/}; }
	symlink_lib() { ln -s ${1%.so*}.so.$version $PKG/$1; }
	symblink_named_lib() { ln -s ${1%.so*}.so.$version $PKG/$2; }


	sh $name-$version.run --extract-only
	cd NVIDIA-Linux-x86_64-$version



	# X driver and GLX extension
	install_lib nvidia_drv.so xorg/modules/drivers/
	install_lib libglxserver_nvidia.so.$version nvidia/xorg/
	symlink_lib /usr/lib/nvidia/xorg/libglxserver_nvidia.so
	symlink_lib /usr/lib/nvidia/xorg/libglxserver_nvidia.so.1


	# EGL and OpenGL ES libraries and config
	install_lib libGLESv1_CM_nvidia.so.$version
	symlink_lib /usr/lib/libGLESv1_CM_nvidia.so.1
	install_lib libGLESv2_nvidia.so.$version
	symlink_lib /usr/lib/libGLESv2_nvidia.so.2
	install_file 10_nvidia.json usr/share/glvnd/egl_vendor.d
	install_lib libnvidia-allocator.so.$version
	symlink_lib /usr/lib/libnvidia-allocator.so.1



	# wayland EGL library and config
	install_lib libnvidia-egl-wayland.so.1.1.17
	install_file 10_nvidia_wayland.json usr/share/egl/egl_external_platform.d/


	# GLVND vendor implementation libraries
	install_lib libGLX_nvidia.so.$version
	symlink_lib /usr/lib/libGLX_nvidia.so.0
	install_lib libEGL_nvidia.so.$version
	symlink_lib /usr/lib/libEGL_nvidia.so.0

	# vulkan ICD config
	install_file nvidia_icd.json etc/vulkan/icd.d



	# driver component libraries
	install_lib libnvidia-cfg.so.$version
	install_lib libnvidia-eglcore.so.$version
	install_lib libnvidia-glcore.so.$version
	install_lib libnvidia-glsi.so.$version
	install_lib libnvidia-glvkspirv.so.$version
	install_lib libnvidia-gpucomp.so.$version
	install_lib libnvidia-rtcore.so.$version



	# vendor VDPAU library
	install_lib libvdpau_nvidia.so.$version vdpau
	symlink_lib /usr/lib/vdpau/libvdpau_nvidia.so.1
	symlink_lib /usr/lib/vdpau/libvdpau_nvidia.so



	# CUDA libraries
	install_lib libcuda.so.$version
	symlink_lib /usr/lib/libcuda.so.1
	symlink_lib /usr/lib/libcuda.so
	install_lib libcudadebugger.so.$version
	symlink_lib /usr/lib/libcudadebugger.so.1
	symlink_lib /usr/lib/libcudadebugger.so
	install_lib libnvidia-ptxjitcompiler.so.$version
	symlink_lib /usr/lib/libnvidia-ptxjitcompiler.so.1
	symlink_lib /usr/lib/libnvidia-ptxjitcompiler.so

	# OpenCL libraries and config
	install_lib libOpenCL.so.1.0.0
	install_lib libnvidia-opencl.so.$version
	symlink_lib /usr/lib/libnvidia-opencl.so.1
	symlink_lib /usr/lib/libnvidia-opencl.so
	install_file nvidia.icd etc/OpenCL/vendors

	# CUDA MPI applications
	install_bin nvidia-cuda-mps-control
	install_bin nvidia-cuda-mps-server

    
	#GBM
	mkdir -p $PKG/usr/lib/gbm
	symblink_named_lib libnvidia-allocator.so.$version usr/lib/gbm/nvidia-drm_gbm.so
	install_lib libnvidia-egl-gbm.so.1.1.2
	symlink_lib /usr/lib/libnvidia-egl-gbm.so.1
	symlink_lib /usr/lib/libnvidia-egl-gbm.so
	install_file 15_nvidia_gbm.json usr/share/egl/egl_external_platform.d/
	

	#NGX
	install_lib libnvidia-ngx.so.$version
	symlink_lib /usr/lib/libnvidia-ngx.so.1
	symlink_lib /usr/lib/libnvidia-ngx.so
	

	#WINE
	install_lib nvngx.dll nvidia/wine
	install_lib _nvngx.dll nvidia/wine

	# TLS libraries
	install_lib libnvidia-tls.so.$version
	install_lib libnvidia-pkcs11.so.$version
	install_lib libnvidia-pkcs11-openssl3.so.$version


	# monitoring and management API library
	install_lib libnvidia-ml.so.$version
	# nvidia api
	install_lib libnvidia-api.so.1

	# applications
	install_file nvidia-modprobe usr/bin 4755
	for xx in xconfig settings smi debugdump persistenced; do
		install_bin nvidia-$xx
	done

	# nvidia-settings UI libraries
	install_lib libnvidia-gtk2.so.$version
	install_lib libnvidia-gtk3.so.$version
	
	# encoding/decoding libraries
	install_lib libnvcuvid.so.$version
	install_lib libnvidia-encode.so.$version
	install_lib libnvidia-fbc.so.$version

	# OptiX ray tracing engine
	install -Dm755 nvoptix.bin $PKG/usr/share/nvidia/nvoptix.bin
	install_lib libnvoptix.so.$version
	symlink_lib /usr/lib/libnvoptix.so.1
	symlink_lib /usr/lib/libnvoptix.so

	# X driver config
	install_file $SRC/10-nvidia-drm-outputclass.conf usr/share/X11/xorg.conf.d

	# application profile keys and documentation
	install_file nvidia-application-profiles-$version-rc usr/share/nvidia/
	install_file nvidia-application-profiles-$version-key-documentation usr/share/nvidia/

	# desktop file and icon
	sed -i -e 's,__UTILS_PATH__,/usr/bin,' -e 's,__PIXMAP_PATH__,/usr/share/pixmaps,' nvidia-settings.desktop
	install_file nvidia-settings.desktop usr/share/applications/
	install_file nvidia-settings.png usr/share/pixmaps/

	# dkms modules
	sed -i "s/__VERSION_STRING/$version/" kernel/dkms.conf
	sed -i 's/__JOBS/`nproc`/' kernel/dkms.conf
	sed -i 's/__DKMS_MODULES//' kernel/dkms.conf
	sed -i '$iBUILT_MODULE_NAME[0]="nvidia"\
DEST_MODULE_LOCATION[0]="/kernel/drivers/video"\
BUILT_MODULE_NAME[1]="nvidia-uvm"\
DEST_MODULE_LOCATION[1]="/kernel/drivers/video"\
BUILT_MODULE_NAME[2]="nvidia-modeset"\
DEST_MODULE_LOCATION[2]="/kernel/drivers/video"\
BUILT_MODULE_NAME[3]="nvidia-drm"\
DEST_MODULE_LOCATION[3]="/kernel/drivers/video"' kernel/dkms.conf

	install -dm 755 $PKG/usr/src
	cp -dr --no-preserve='ownership' kernel $PKG/usr/src/nvidia-$version

	# blacklist conflict module
	install -d $PKG/etc/modprobe.d
	echo "blacklist nouveau" > $PKG/etc/modprobe.d/nvidia.conf

	# dkms build modules script
	mkdir -p $PKG/var/lib/dkms
	sed -e "s/@name@/$name/g" \
	    -e "s/@version@/$version/g" \
	$SRC/buildmodules-$name.sh > $PKG/var/lib/dkms/buildmodules-$name.sh

	# prime-run script
	install -Dm744 $SRC/prime-run $PKG/usr/bin/prime-run


	#optical flow
	install_lib libnvidia-opticalflow.so.$version
	symlink_lib /usr/lib/libnvidia-opticalflow.so.1
	symlink_lib /usr/lib/libnvidia-opticalflow.so

	#firmware
	#(firmware/nvidia/$version/gsp_*.bin)
	install_file firmware/gsp_ga10x.bin lib/firmware/nvidia/$version/
	install_file firmware/gsp_tu10x.bin lib/firmware/nvidia/$version/

	#nvvm
	install_lib libnvidia-nvvm.so.$version
	symlink_lib /usr/lib/libnvidia-nvvm.so.1
	symlink_lib /usr/lib/libnvidia-nvvm.so
}
